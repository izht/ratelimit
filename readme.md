# Hotel API & Rate limit
### Constraint
- API calls need to be rate limited (request per 10 seconds)
- On exceeding the limit, api key must be suspended for next 5 minutes. 
- API key can have different rate limit set from configuration, and global rate limit for key which not in configuration
- Search hotels by CityId
- Provide optional sorting of the result by Price (both ASC and DESC order).

### Program Description
- This project was implemented using Java, Spring Boot, Spring Data JPA and H2 Database
- We need to generate id for city, I choose in-memory database to store City and Hotel. So this database take responsibility to generate the city id.
- Also for sorting the price, Spring Data JPA work perfectly for this job. By just creating the Interface of the Repository. All the query was done inside the Spring Data Framework.
- Assuming API Key can be any String, only predefined key can have limit set in `limit.properties` file. Other than that, the default limit is `3`
- The RateLimit was implemented using Java ConcurrentHashMap

### API
If you run the JAR, you can access this following path in the browser.

List all city
```
localhost:8080/cities?api_key=xxx
```

Find city by name
```
localhost:8080/city?api_key=xxx&name=bangkok
```

Search hotel by city id
```
localhost:8080/hotels?api_key=xxx&city_id=1
```

### How to run
Download the JAR from https://bitbucket.org/izht/ratelimit/downloads/ratelimit-1.0.jar then run
```sh
java -jar ratelimit-1.0.jar
```
You also can build new JAR from source code using `mvn package` or run directly from source `mvn spring-boot:run`