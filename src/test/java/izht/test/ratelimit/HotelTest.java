package izht.test.ratelimit;

import static org.junit.Assert.assertEquals;
import izht.agoda.ratelimit.Application;
import izht.agoda.ratelimit.model.City;
import izht.agoda.ratelimit.model.Hotel;
import izht.agoda.ratelimit.service.HotelAndCityService;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Hotel Test.
 * 
 * @author Boonyachart Kanchanabul
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class HotelTest {

   @Autowired
   private HotelAndCityService hotelAndCityService;

   @Test
   public void testListCity() {
      List<City> cities = hotelAndCityService.listCity();
      assertEquals("There must be 3 cities", 3, cities.size());
   }

   @Test
   public void testFindCity() {
      City city = hotelAndCityService.findCityByName("bangkok");
      assertEquals("Must get Bangkok City", "Bangkok", city.getName());
   }

   @Test
   public void testSearchHotelByCity() {
      City city = hotelAndCityService.findCityByName("bangkok");
      List<Hotel> hotels = hotelAndCityService.listHotelByCityId(city.getId(), null);
      assertEquals("There must be 7 hotel in Bangkok", 7, hotels.size());
      for (Hotel hotel : hotels) {
         assertEquals("Hotel must be in Bangkok", "Bangkok", hotel.getCity().getName());
      }
   }
}
