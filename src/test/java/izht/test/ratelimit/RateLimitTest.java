package izht.test.ratelimit;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import izht.agoda.ratelimit.Application;
import izht.agoda.ratelimit.controller.ApiController;
import izht.agoda.ratelimit.model.ApiKey;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Rate limited test.
 * 
 * @author Boonyachart Kanchanabul
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class RateLimitTest {

   @Autowired
   private WebApplicationContext webApplicationContext;

   private MockMvc mockMvc;

   @Before
   public void setup() {
      this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
   }

   @Test
   public void testUnknownKey() throws Exception {
      String unknown = "x";
      for (int i = 0; i < ApiController.GLOBAL_RATE_LIMIT; i++)
         mockMvc.perform(get("/cities").param("api_key", unknown)).andExpect(status().is2xxSuccessful());
      mockMvc.perform(get("/cities").param("api_key", unknown)).andExpect(status().isUnauthorized());
   }

   @Test
   public void testWaitBeforeSuspend() throws Exception {
      String unknown = "y";
      for (int i = 0; i < ApiController.GLOBAL_RATE_LIMIT; i++)
         mockMvc.perform(get("/cities").param("api_key", unknown)).andExpect(status().is2xxSuccessful());
      Thread.sleep(10000); // wait 10 seconds
      mockMvc.perform(get("/cities").param("api_key", unknown)).andExpect(status().is2xxSuccessful());
   }

   @Test
   public void testProviedKey() throws Exception {
      String provided = "limit100";
      for (int i = 0; i < 100; i++)
         mockMvc.perform(get("/cities").param("api_key", provided)).andExpect(status().is2xxSuccessful());
      mockMvc.perform(get("/cities").param("api_key", provided)).andExpect(status().isUnauthorized());
   }

   @Test
   public void testSuspendTime() {
      Date date = new Date();
      ApiKey apiKey = new ApiKey("test", 2);
      assertTrue("First call will success", apiKey.increaseCount(date.getTime()));
      date = DateUtils.addSeconds(date, 1);
      assertTrue("Second call will success", apiKey.increaseCount(date.getTime()));
      date = DateUtils.addSeconds(date, 1);
      assertFalse("Third call will fail", apiKey.increaseCount(date.getTime()));

      date = DateUtils.addMinutes(date, 5);
      assertFalse("After suspended it will be fail for 5 minute", apiKey.increaseCount(date.getTime()));

      date = DateUtils.addMinutes(date, 5);
      date = DateUtils.addMilliseconds(date, 1);
      assertTrue("After 5 minute of suspended it will become available again", apiKey.increaseCount(date.getTime()));
   }
}
