package izht.agoda.ratelimit.response;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class RestResponse {

   private int status;

   private String message;

   private Object data;

   public RestResponse(int status, String message, Object data) {
      this.status = status;
      this.message = message;
      this.data = data;
   }

   public static RestResponse ok(Object data) {
      return new RestResponse(200, "success", data);
   }

   public static RestResponse notFound(String message, HttpServletResponse response) {
      response.setStatus(HttpServletResponse.SC_NOT_FOUND);
      return new RestResponse(HttpServletResponse.SC_NOT_FOUND, message, null);
   }

   public static RestResponse error(int status, String message) {
      return new RestResponse(status, message, null);
   }

   public static RestResponse suspended(String message, HttpServletResponse response) {
      response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      return new RestResponse(HttpServletResponse.SC_UNAUTHORIZED, message, null);
   }

   /**
    * Gets status.
    * 
    * @return the status
    */
   public int getStatus() {
      return status;
   }

   /**
    * Set status.
    * 
    * @param status the status to set
    */
   public void setStatus(int status) {
      this.status = status;
   }

   /**
    * Gets message.
    * 
    * @return the message
    */
   public String getMessage() {
      return message;
   }

   /**
    * Set message.
    * 
    * @param message the message to set
    */
   public void setMessage(String message) {
      this.message = message;
   }

   /**
    * Gets data.
    * 
    * @return the data
    */
   public Object getData() {
      return data;
   }

   /**
    * Set data.
    * 
    * @param data the data to set
    */
   public void setData(Object data) {
      this.data = data;
   }

}
