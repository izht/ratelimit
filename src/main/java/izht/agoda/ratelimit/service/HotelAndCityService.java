package izht.agoda.ratelimit.service;

import izht.agoda.ratelimit.model.City;
import izht.agoda.ratelimit.model.Hotel;

import java.util.List;

public interface HotelAndCityService {

   List<City> listCity();

   City findCityByName(String cityName);

   List<Hotel> listHotelByCityId(Integer cityId, String sort);

}
