package izht.agoda.ratelimit.service.impl;

import izht.agoda.ratelimit.model.City;
import izht.agoda.ratelimit.model.Hotel;
import izht.agoda.ratelimit.repository.CityRepository;
import izht.agoda.ratelimit.repository.HotelRepository;
import izht.agoda.ratelimit.service.HotelAndCityService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HotelAndCityServiceImpl implements HotelAndCityService {

   @Autowired
   private HotelRepository hotelRepository;

   @Autowired
   private CityRepository cityRepository;

   @Override
   public List<City> listCity() {
      return (List<City>)cityRepository.findAll();
   }

   @Override
   public City findCityByName(String cityName) {
      return cityRepository.findOneByNameIgnoreCase(cityName);
   }

   @Override
   public List<Hotel> listHotelByCityId(Integer cityId, String sort) {
      if ("DESC".equalsIgnoreCase(sort))
         return hotelRepository.findByCityIdOrderByPriceDesc(cityId);
      else if ("ASC".equalsIgnoreCase(sort))
         return hotelRepository.findByCityIdOrderByPriceAsc(cityId);
      else
         return hotelRepository.findByCityId(cityId);
   }
}
