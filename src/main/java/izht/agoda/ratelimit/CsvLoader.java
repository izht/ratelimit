package izht.agoda.ratelimit;

import izht.agoda.ratelimit.model.City;
import izht.agoda.ratelimit.model.Hotel;
import izht.agoda.ratelimit.repository.CityRepository;
import izht.agoda.ratelimit.repository.HotelRepository;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * Load CSV data to in-memory database when application start.
 * 
 * @author Boonyachart Kanchanabul
 */
@Component
public class CsvLoader implements CommandLineRunner {

   private static final String CSV_FILENAME = "hoteldb.csv";

   private static Logger logger = LoggerFactory.getLogger(CsvLoader.class);

   @Autowired
   private CityRepository cityRepository;

   @Autowired
   private HotelRepository hotelRepository;

   @Override
   public void run(String... arg0) throws Exception {
      loadCsvToInMemoryDatabase();
   }

   public void loadCsvToInMemoryDatabase() {
      Map<String, City> cityMap = new HashMap<>();
      try (InputStream stream = new ClassPathResource(CSV_FILENAME).getInputStream(); Scanner scanner = new Scanner(stream)) {
         scanner.nextLine(); // skip first line
         int line = 1;
         while (scanner.hasNextLine()) {
            ++line;
            try {
               String[] values = scanner.nextLine().split(",");
               String cityName = values[0];
               Integer hotelId = Integer.parseInt(values[1]);
               String roomName = values[2];
               Integer price = Integer.parseInt(values[3]);

               City city = cityMap.get(cityName);
               if (city == null) {
                  city = cityRepository.save(new City(cityName));
                  cityMap.put(cityName, city);
                  logger.debug("{} city save as id {}", cityName, city.getId());
               }
               hotelRepository.save(new Hotel(hotelId, roomName, price, city));
            }
            catch (Exception e) {
               logger.error("Cannot read data from CSV line {} : {}", line, e.getMessage());
            }
         }
         scanner.close();
      }
      catch (IOException e) {
         logger.error(e.getMessage(), e);
      }
   }
}
