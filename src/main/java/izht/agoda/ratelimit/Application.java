package izht.agoda.ratelimit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application entry point.
 * 
 * @author Boonyachart Kanchanabul
 *
 */
@SpringBootApplication
public class Application {

   public static void main(String[] args) {
      SpringApplication.run(Application.class);
   }

}
