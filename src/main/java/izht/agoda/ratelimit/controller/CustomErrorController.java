package izht.agoda.ratelimit.controller;

import izht.agoda.ratelimit.response.RestResponse;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Error controller for json response.
 * 
 * @author Boonyachart Kanchanabul
 */
@RestController
public class CustomErrorController implements ErrorController {

   private static final String PATH = "/error";

   @Override
   public String getErrorPath() {
      return PATH;
   }

   @Autowired
   private ErrorAttributes errorAttributes;

   @RequestMapping(value = PATH)
   public RestResponse error(HttpServletRequest request, HttpServletResponse response) {
      Map<String, Object> error = errorAttributes.getErrorAttributes(new ServletRequestAttributes(request), false);
      return RestResponse.error(response.getStatus(), error.get("error").toString() + " : " + error.get("message").toString());
   }

}
