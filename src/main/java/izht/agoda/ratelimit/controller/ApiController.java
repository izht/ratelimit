package izht.agoda.ratelimit.controller;

import izht.agoda.ratelimit.model.ApiKey;
import izht.agoda.ratelimit.model.City;
import izht.agoda.ratelimit.model.Hotel;
import izht.agoda.ratelimit.response.RestResponse;
import izht.agoda.ratelimit.service.HotelAndCityService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest API Controller.
 * 
 * @author Boonyachart Kanchanabul
 */
@RestController
public class ApiController {

   public static final int GLOBAL_RATE_LIMIT = 3;
   private static Logger logger = LoggerFactory.getLogger(ApiController.class);
   private static final String LIMIT_CONFIG_FILENAME = "limit.properties";

   private static ConcurrentHashMap<String, ApiKey> keys = new ConcurrentHashMap<String, ApiKey>();

   private Properties limitRateConfig = new Properties();

   @PostConstruct
   private void init() {
      try (InputStream input = new ClassPathResource(LIMIT_CONFIG_FILENAME).getInputStream()) {
         limitRateConfig.load(input);
      }
      catch (IOException e) {
         logger.error(e.getMessage(), e);
      }
   }

   @Autowired
   private HotelAndCityService hotelAndCityService;

   /**
    * List all cities.
    * 
    * @param key API Key
    * @param response HttpServletResponse
    * @return list of all cities
    */
   @RequestMapping(value = "/cities")
   public RestResponse findCity(@RequestParam(value = "api_key") String key, HttpServletResponse response) {
      if (!verifyKey(key))
         return RestResponse.suspended("API Key limit exceeded", response);
      return RestResponse.ok(hotelAndCityService.listCity());
   }

   /**
    * Find city.
    * 
    * @param key API Key
    * @param cityName city name
    * @param response HttpServletResponse
    * @return city
    */
   @RequestMapping(value = "/city")
   public RestResponse findCity(@RequestParam(value = "api_key") String key, @RequestParam(value = "name") String cityName,
                                HttpServletResponse response) {
      if (!verifyKey(key))
         return RestResponse.suspended("API Key limit exceeded", response);

      City city = hotelAndCityService.findCityByName(cityName);
      if (city != null)
         return RestResponse.ok(city);
      else
         return RestResponse.notFound("Cannot find city : " + cityName, response);
   }

   /**
    * Search hotel by city id.
    * 
    * @param key API Key
    * @param cityId city id
    * @param sort (optional) for sort hotel by price, can be ASC or DESC
    * @param response HttpServletResponse
    * @return list of hotel in the city
    */
   @RequestMapping(value = "/hotels")
   public RestResponse searchHotelByCityId(@RequestParam(value = "api_key") String key, @RequestParam(value = "city_id") Integer cityId,
                                           @RequestParam(value = "sort", required = false) String sort, HttpServletResponse response) {
      if (!verifyKey(key))
         return RestResponse.suspended("API Key limit exceeded", response);

      List<Hotel> hotels = hotelAndCityService.listHotelByCityId(cityId, sort);
      if (hotels != null)
         return RestResponse.ok(hotels);
      else
         return RestResponse.notFound("Cannot find hotel in city id : " + cityId, response);
   }

   private boolean verifyKey(String apiKey) {
      if (!keys.containsKey(apiKey)) {
         String limitFromConfig = limitRateConfig.getProperty(apiKey);
         int limit = limitFromConfig == null ? GLOBAL_RATE_LIMIT : Integer.parseInt(limitFromConfig);
         logger.debug("Register new API Key : {}, limit : {}", apiKey, limit);
         keys.putIfAbsent(apiKey, new ApiKey(apiKey, limit));
      }
      logger.debug("API Key : {}", keys.get(apiKey));
      return keys.get(apiKey).increaseCount(); // synchronizely count request and check limit
   }
}
