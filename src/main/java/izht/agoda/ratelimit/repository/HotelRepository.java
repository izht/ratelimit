package izht.agoda.ratelimit.repository;

import izht.agoda.ratelimit.model.Hotel;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface HotelRepository extends CrudRepository<Hotel, Long> {

   List<Hotel> findByCityId(Integer cityId);

   List<Hotel> findByCityIdOrderByPriceAsc(Integer cityId);

   List<Hotel> findByCityIdOrderByPriceDesc(Integer cityId);
}
