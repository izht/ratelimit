package izht.agoda.ratelimit.repository;

import izht.agoda.ratelimit.model.City;

import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<City, Integer> {

   City findOneByNameIgnoreCase(String name);
}
