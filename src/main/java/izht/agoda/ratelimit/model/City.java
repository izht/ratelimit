package izht.agoda.ratelimit.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * City Model.
 * 
 * @author Boonyachart Kanchanabul
 */
@Entity
public class City {

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Integer id;

   private String name;

   public City() {
      // empty constructor
   }

   public City(String name) {
      this.name = name;
   }

   /**
    * Gets id.
    *
    * @return the id
    */
   public Integer getId() {
      return id;
   }

   /**
    * Set id.
    *
    * @param id the id to set
    */
   public void setId(Integer id) {
      this.id = id;
   }

   /**
    * Gets name.
    *
    * @return the name
    */
   public String getName() {
      return name;
   }

   /**
    * Set name.
    *
    * @param name the name to set
    */
   public void setName(String name) {
      this.name = name;
   }

}
