package izht.agoda.ratelimit.model;

/**
 * API Key.
 * 
 * @author Boonyachart Kanchanabul
 *
 */
public class ApiKey {

   private static final long MILLISEC_OF_LIMIT_REQUEST = 10 * 1000; // 10 second
   private static final long MILLISEC_OF_SUSPENDED_TIME = 5 * 60 * 1000; // 5 minute

   private String id;

   private int limit;

   private int count;

   private long timestamp;

   public ApiKey() {
      // empty constructor
   }

   public ApiKey(String id, int limit) {
      this.id = id;
      this.limit = limit;
   }

   /**
    * Increase number of API Key used and suspend if its exceeded limit.
    * 
    * @return true if API Key not suspended
    */
   public synchronized boolean increaseCount() {
      return increaseCount(System.currentTimeMillis());
   }

   /**
    * Increase number of API Key used and suspend if its exceeded limit.
    * 
    * @param newTimestamp timestamp of the new request
    * @return true if API Key not suspended
    */
   public synchronized boolean increaseCount(long newTimestamp) {
      if (count > limit && newTimestamp - timestamp <= MILLISEC_OF_SUSPENDED_TIME) {
         // suspending don't reset count yet
      } else if (newTimestamp - timestamp > MILLISEC_OF_LIMIT_REQUEST)
         count = 0; // reset count when 10 second passed
      count++;
      this.timestamp = newTimestamp;
      return count <= limit;
   }

   @Override
   public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("ApiKey [id=");
      builder.append(id);
      builder.append(", limit=");
      builder.append(limit);
      builder.append(", count=");
      builder.append(count);
      builder.append(", timestamp=");
      builder.append(timestamp);
      builder.append("]");
      return builder.toString();
   }

}
