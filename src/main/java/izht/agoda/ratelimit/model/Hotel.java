package izht.agoda.ratelimit.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Hotel Model.
 * 
 * @author Boonyachart Kanchanabul
 */
@Entity
public class Hotel {

   @Id
   private Integer id;

   private String roomName;

   private Integer price;

   @ManyToOne
   @JoinColumn(name = "city_id")
   private City city;

   public Hotel() {
      // empty constructor
   }

   public Hotel(Integer id, String roomName, Integer price, City city) {
      this.id = id;
      this.roomName = roomName;
      this.price = price;
      this.city = city;
   }

   /**
    * Gets id.
    *
    * @return the id
    */
   public Integer getId() {
      return id;
   }

   /**
    * Set id.
    *
    * @param id the id to set
    */
   public void setId(Integer id) {
      this.id = id;
   }

   /**
    * Gets roomName.
    *
    * @return the roomName
    */
   public String getRoomName() {
      return roomName;
   }

   /**
    * Set roomName.
    *
    * @param roomName the roomName to set
    */
   public void setRoomName(String roomName) {
      this.roomName = roomName;
   }

   /**
    * Gets price.
    *
    * @return the price
    */
   public Integer getPrice() {
      return price;
   }

   /**
    * Set price.
    *
    * @param price the price to set
    */
   public void setPrice(Integer price) {
      this.price = price;
   }

   /**
    * Gets city.
    *
    * @return the city
    */
   public City getCity() {
      return city;
   }

   /**
    * Set city.
    *
    * @param city the city to set
    */
   public void setCity(City city) {
      this.city = city;
   }

}
